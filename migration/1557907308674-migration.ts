import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1557907308674 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `metro` ADD `metroName` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `advert` DROP FOREIGN KEY `FK_de9fd46bb455db899e69e0a0ae3`");
        await queryRunner.query("ALTER TABLE `metro` DROP PRIMARY KEY");
        await queryRunner.query("ALTER TABLE `metro` DROP COLUMN `id`");
        await queryRunner.query("ALTER TABLE `metro` ADD `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT");
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `metroId`");
        await queryRunner.query("ALTER TABLE `advert` ADD `metroId` int NULL");
        await queryRunner.query("ALTER TABLE `advert` ADD CONSTRAINT `FK_de9fd46bb455db899e69e0a0ae3` FOREIGN KEY (`metroId`) REFERENCES `metro`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `advert` DROP FOREIGN KEY `FK_de9fd46bb455db899e69e0a0ae3`");
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `metroId`");
        await queryRunner.query("ALTER TABLE `advert` ADD `metroId` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `metro` DROP COLUMN `id`");
        await queryRunner.query("ALTER TABLE `metro` ADD `id` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `metro` ADD PRIMARY KEY (`id`)");
        await queryRunner.query("ALTER TABLE `advert` ADD CONSTRAINT `FK_de9fd46bb455db899e69e0a0ae3` FOREIGN KEY (`metroId`) REFERENCES `metro`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `metro` DROP COLUMN `metroName`");
    }

}
