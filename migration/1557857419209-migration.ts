import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1557857419209 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `picture` (`id` int NOT NULL AUTO_INCREMENT, `url` varchar(255) NOT NULL, `fileName` varchar(255) NULL, `soft` int NOT NULL, `advertId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `metro` (`id` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `advert` (`id` int NOT NULL AUTO_INCREMENT, `advertId` varchar(255) NOT NULL, `siteName` varchar(255) NULL, `url` varchar(255) NULL, `metroName` varchar(255) NULL, `sellerName` varchar(255) NULL, `content` longtext NULL, `address` varchar(255) NULL, `cityName` varchar(255) NULL, `type` varchar(255) NULL, `subtype` varchar(255) NULL, `act` varchar(255) NULL, `price` decimal NULL, `phone` varchar(255) NULL, `sellerType` varchar(255) NULL, `phonesCount` int NULL, `floor` tinyint NULL, `floors` tinyint NULL, `areaAll` float NULL, `areaKitchen` float NULL, `areaLive` float NULL, `rooms` tinyint NULL, `latitude` double NULL, `longitude` double NULL, `district` varchar(255) NULL, `street` varchar(255) NULL, `house` varchar(255) NULL, `mainPhotoUrl` varchar(255) NULL, `cityId` int NULL, `reatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `metroId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `picture` ADD CONSTRAINT `FK_dd6e4eca91c7f4ec610a26abac5` FOREIGN KEY (`advertId`) REFERENCES `advert`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `advert` ADD CONSTRAINT `FK_de9fd46bb455db899e69e0a0ae3` FOREIGN KEY (`metroId`) REFERENCES `metro`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `advert` DROP FOREIGN KEY `FK_de9fd46bb455db899e69e0a0ae3`");
        await queryRunner.query("ALTER TABLE `picture` DROP FOREIGN KEY `FK_dd6e4eca91c7f4ec610a26abac5`");
        await queryRunner.query("DROP TABLE `advert`");
        await queryRunner.query("DROP TABLE `metro`");
        await queryRunner.query("DROP TABLE `picture`");
    }

}
