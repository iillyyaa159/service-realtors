import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1557949419284 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `cityName`");
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `mainPhotoUrl`");
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `metroName`");
        await queryRunner.query("ALTER TABLE `advert` ADD `mainPhotoId` int NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `advert` DROP COLUMN `mainPhotoId`");
        await queryRunner.query("ALTER TABLE `advert` ADD `metroName` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `advert` ADD `mainPhotoUrl` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `advert` ADD `cityName` varchar(255) NULL");
    }

}
