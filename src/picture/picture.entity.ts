import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, PrimaryColumn } from 'typeorm';
import { Advert } from '../advert/advert.entity';

@Entity()
export class Picture {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    url: string;

    @Column({ nullable: true })
    fileName: string;

    @Column()
    soft: number;

    @ManyToOne(type => Advert, advert => advert.pictures)
    advert: Advert;

    async parsingData(urlPicture: any, soft: number): Promise<Picture> {
        this.url = urlPicture;
        this.soft = soft;
        this.fileName = '';

        return this;
    }
}
