import { Module } from '@nestjs/common';
import { PictureService } from './picture.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Picture } from './picture.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Picture])],
  providers: [PictureService],
  exports: [PictureService],
})
export class PictureModule {}
