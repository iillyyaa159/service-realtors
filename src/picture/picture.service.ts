import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import * as request from 'request-promise-native';
import { Repository } from 'typeorm';
import { Sleep } from '../helpers/sleep';
import { Picture } from './picture.entity';
import { promisify } from 'util';

const SLEEP = 1 * 60 * 1000;

const writeFile = promisify(fs.writeFile);

@Injectable()
export class PictureService {
    constructor(
        @InjectRepository(Picture)
        private readonly repositoryPicture: Repository<Picture>,
    ) {
        this.savePicture();
    }

    async getNoDownloads(): Promise<Picture> {
        return await this.repositoryPicture.findOne({
            relations: ['advert'],
            where: { fileName: '' },
        });
    }

    async savePicture() {
        while (true) {
            try {
                const picture = await this.getNoDownloads();

                if (!picture) {
                    console.log('Картинки закончились');
                    await Sleep(SLEEP);
                    continue;
                }

                if (!picture.advert) {
                    console.log('Нет id объявления');
                    continue;
                }

                console.time('Время сохранения картинки');

                let path = `./pictures/`;

                if (!fs.existsSync(path)) {
                    fs.mkdirSync(path);
                }

                path += picture.advert.id;

                if (!fs.existsSync(path)) {
                    fs.mkdirSync(path);
                }

                const arrayUrl = picture.url.split('/');
                const fileNameTemp = arrayUrl[arrayUrl.length - 1];

                if (fileNameTemp.split('.').length < 2) {
                    picture.fileName = `${picture.soft}.jpg`;
                } else {
                    picture.fileName = picture.soft + '.' + fileNameTemp.split('.')[1];
                }

                try {
                    const body = await request(picture.url, { encoding: 'binary' });
                    await writeFile(path + '/' + picture.fileName, body, 'binary');
                } catch (err) {
                    picture.fileName = 'error';
                }

                await this.repositoryPicture.save(picture);

                console.timeEnd('Время сохранения картинки');
                console.log('Кратинка сохранилась');
            } catch (err) {
                console.error(err);
            }
        }
    }
}
