import { Entity, PrimaryColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Advert } from '../advert/advert.entity';

@Entity()
export class Metro {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    metroName: string;

    @OneToMany(type => Advert, advert => advert.metro)
    adverts: Advert[];

    async parsingData(metroName) {
        this.metroName = metroName;

        return this;
    }
}
