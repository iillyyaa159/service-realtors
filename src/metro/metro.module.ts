import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Metro } from './metro.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Metro])],
})
export class MetroModule {}
