import { Module } from '@nestjs/common';
import { AdvertModule } from './advert/advert.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PictureModule } from './picture/picture.module';
import { MetroModule } from './metro/metro.module';

@Module({
  imports: [TypeOrmModule.forRoot(), AdvertModule, PictureModule, MetroModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
