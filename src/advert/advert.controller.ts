import { Body, Controller, HttpCode, Post, Res, Header, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AdvertService } from './advert.service';

@Controller('adverts')
export class AdvertController {
  constructor(
    private advertService: AdvertService,
  ) {}

  @Post()
  @HttpCode(200)
  @UseInterceptors(FileInterceptor('file'))
  async create(@Body() modelData) {
    console.time(`Время сохранения данных в очередь`);
    const dataJson = JSON.parse(modelData.data);
    await this.advertService.saveMulti(dataJson);
    console.timeEnd(`Время сохранения данных в очередь`);
  }
}
