import { Entity, Column, OneToMany, UpdateDateColumn, CreateDateColumn, ManyToOne, PrimaryGeneratedColumn, getConnection } from 'typeorm';
import { Picture } from '../picture/picture.entity';
import { Metro } from '../metro/metro.entity';

@Entity()
export class Advert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  advertId: string;

  @Column({nullable: true})
  siteName: string;

  @Column({nullable: true})
  url: string;

  @Column({nullable: true})
  sellerName: string;

  @Column({type: 'longtext', nullable: true})
  content: string;

  @Column({nullable: true})
  address: string;

  @Column({nullable: true})
  type: string;

  @Column({nullable: true})
  subtype: string;

  @Column({nullable: true})
  act: string;

  @Column({type: 'decimal', nullable: true})
  price: number;

  @Column({nullable: true})
  phone: string;

  @Column({nullable: true})
  sellerType: string;

  @Column({nullable: true})
  phonesCount: number;

  @Column({type: 'tinyint', nullable: true})
  floor: number;

  @Column({type: 'tinyint', nullable: true})
  floors: number;

  @Column({type: 'float', nullable: true})
  areaAll: number;

  @Column({type: 'float', nullable: true})
  areaKitchen: number;

  @Column({type: 'float', nullable: true})
  areaLive: number;

  @Column({type: 'tinyint', nullable: true})
  rooms: number;

  @Column({type: 'double', nullable: true})
  latitude: number;

  @Column({type: 'double', nullable: true})
  longitude: number;

  @Column({nullable: true})
  district: string;

  @Column({nullable: true})
  street: string;

  @Column({nullable: true})
  house: string;

  @Column({nullable: true})
  mainPhotoId: number;

  @Column({nullable: true})
  cityId: number;

  @CreateDateColumn({type: 'timestamp'})
  reatedAt: Date;

  @UpdateDateColumn({type: 'timestamp'})
  updatedAt: Date;

  @OneToMany(type => Picture, picture => picture.advert, {
    cascade: true,
    nullable: true,
  })
  pictures: Picture[];

  @ManyToOne(type => Metro, metro => metro.adverts, {
    cascade: true,
    nullable: true,
  })
  metro: Metro;

  async parsingData(advertBody: any): Promise<Advert> {
    this.advertId = advertBody.id.toString();
    this.siteName = advertBody.site_name;
    this.url = advertBody.url;
    this.sellerName = advertBody.seller_name;
    this.content = advertBody.content;
    this.address = advertBody.address;
    this.type = advertBody.type;
    this.subtype = advertBody.subtype;
    this.act = advertBody.act;
    this.price = advertBody.price;
    this.phone = advertBody.phone;
    this.sellerType = advertBody.seller_type;
    this.phonesCount = advertBody.phones_count;
    this.floor = advertBody.floor;
    this.floors = advertBody.floors;
    this.areaAll = advertBody.area_all;
    this.areaKitchen = advertBody.area_kitchen;
    this.areaLive = advertBody.area_live;
    this.rooms = advertBody.rooms;
    this.latitude = advertBody.latitude;
    this.longitude = advertBody.longitude;
    this.district = advertBody.district;
    this.street = advertBody.street;
    this.house = advertBody.house;

    this.cityId = 4;

    if (advertBody.metro) {
      if (this.metro) {
        this.metro = await this.metro.parsingData(advertBody.metro);
      } else {
        const metroTemp = await getConnection().getRepository(Metro).findOne({
          where: {
            metroName: advertBody.metro,
          },
        });
        if (metroTemp) {
          this.metro = await metroTemp.parsingData(advertBody.metro);
        } else {
          this.metro = await new Metro().parsingData(advertBody.metro);
        }
      }
    }

    if (advertBody.photo_urls) {

      let soft = 0;

      let pictureTemp;
      if (this.pictures) {
        pictureTemp = Object.assign({}, this.pictures);
      }
      this.pictures = Array<Picture>();

      for (const urlPicture of advertBody.photo_urls) {
        let picture;
        if (pictureTemp) {
          picture = pictureTemp.find(x => x.url === urlPicture);
        }

        if (picture) {
          this.pictures.push(await picture.parsingData(urlPicture, soft));
        } else {
          this.pictures.push(await new Picture().parsingData(urlPicture, soft));
        }
        soft++;
      }
    }

    return this;
  }
}
