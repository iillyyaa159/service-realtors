import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Advert } from './advert.entity';
import { Repository } from 'typeorm';
import { Sleep } from '../helpers/sleep';

const ADVERTS_ARRAY = Array<Advert>();
const SLEEP = 1 * 60 * 1000;
const SITE_ENABLE = [
  'avito.ru',
  'yandex.ru',
  'cian.ru',
  'youla.ru',
];

@Injectable()
export class AdvertService {
  constructor(
    @InjectRepository(Advert)
    private readonly advertRepository: Repository<Advert>,
  ) {
    this.saveArrayAdverts();
  }

  async saveMulti(advertsData) {
    try {
      for (const advertData of advertsData) {
        if (advertData.seller_type === 'individual'
          && SITE_ENABLE.includes(advertData.site_name)
        ) {
          ADVERTS_ARRAY.push(advertData);
        }
      }
    } catch (err) {
      console.error(err);
    }
  }

  async getAdvert(advertId: string): Promise<Advert> {
    return await this.advertRepository.findOne({
      relations: ['pictures', 'metro'],
      where: { advertId },
    });
  }

  async saveArrayAdverts() {
    while (true) {
      try {
        const item = ADVERTS_ARRAY[0];

        if (!item) {
          console.log('Очередь закончилась');
          await Sleep(SLEEP);
          continue;
        }

        let advert = await this.getAdvert(item.advertId);

        if (advert) {
          advert = await advert.parsingData(item);
        } else {
          advert = await new Advert().parsingData(item);
        }

        const advertTemp = await this.advertRepository.save(advert);

        if (advertTemp.pictures) {
          advertTemp.mainPhotoId = advertTemp.pictures[0].id;
          await await this.advertRepository.save(advert);
        }
      } catch (err) {
        console.error(err);
      }
      ADVERTS_ARRAY.splice(0, 1);
      console.log(`Осталось всего записать ${ADVERTS_ARRAY.length}`);

    }
  }
}
