import { Module } from '@nestjs/common';
import { Advert } from './advert.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdvertService } from './advert.service';
import { AdvertController } from './advert.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Advert])],
  providers: [AdvertService],
  exports: [AdvertService],
  controllers: [AdvertController],
})
export class AdvertModule {}
